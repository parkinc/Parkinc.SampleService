#!/bin/bash

# set environment variables used in deploy.sh and AWS task-definition.json:
export IMAGE_NAME=parkinc-sampleservice
export IMAGE_VERSION=${CI_COMMIT_SHA:0:6}-$CI_JOB_ID

export AWS_DEFAULT_REGION=eu-west-1
export AWS_ECS_CLUSTER_NAME=default
export AWS_VIRTUAL_HOST=sampleservice.parkinc.valorl.com


# set any sensitive information in travis-ci encrypted project settings:
# required: AWS_ACCOUNT_NUMBER, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
# optional: SERVICESTACK_LICENSE
export AWS_ECS_REPO_DOMAIN=$AWS_ACCOUNT_NUMBER.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com
